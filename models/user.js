// import the necessary modules
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var Schema = mongoose.Schema;


// define schema
var UserSchema = new Schema({
    email: String,
    encriptedPasswd: String,
    created_at: Date,
    updated_at: Date,
    address: {
        street: String,
        number: String,
        depto: String,
        cp: String
    }
});

//hook para encriptar el password del usuario cuando se crea
UserSchema.pre('save',
    function (callback) {
        var user = this;
        // Break out if the password hasn't changed
        if (!user.isModified('encriptedPasswd')) return callback();
        // Password changed so we need to hash it
        bcrypt.genSalt(5,
            function (err, salt) {
                if (err) return callback(err);
                bcrypt.hash(user.encriptedPasswd, salt, null, function (err, hash) {
                    if (err) return callback(err);
                    user.encriptedPasswd = hash;
                    callback();
                });
            });
    });

module.exports = mongoose.model('User', UserSchema);
